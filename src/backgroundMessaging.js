
// receiving message from content script
chrome.runtime.onMessage.addListener((request, sender, resp) => {  
    console.log("request: ", request);
    console.log("sender: ", sender);
    console.log("resp: ", resp());
})

// // listener when book mark is moved
// chrome.bookmarks.onMoved.addListener(() => {

//     // here we are sending the message from background to content script
//     chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
//       chrome.tabs.sendMessage(tabs[0].id, { name: 'Tomas' }, () => console.log('aha'));
//     });
// });

chrome.bookmarks.onMoved.addListener(() => {
    // sendingmeesage from background to content script
    chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
      chrome.tabs.sendMessage(tabs[0].id, { name: 'Tomas' }, () => console.log('aha'));
    });
});
